package cadenaUtilTest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tareas.dos.Calculadora.Calculadora;

class CalculadoraTest {
    Calculadora calc = new Calculadora();
    int a=5;
    int b=4;
    int resultadoEsperadoSuma =9;
    int resultadoEsperadoResta=1;
    int resultadoEsperadoMultiplicacion=20;
    String resultadoEsperadoDivisionPorCero;
    int resultadoObtenido;
    String mensajeEsperado="División por cero no permitida";
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void sumar() {
        resultadoObtenido = calc.sumar(a,b);
        Assertions.assertEquals(resultadoEsperadoSuma, calc.sumar(a,b));
    }

    @Test
    void restar() {
        resultadoObtenido = calc.restar(a,b);
        Assertions.assertEquals(resultadoEsperadoResta, calc.restar(a,b));
    }

    @Test
    void multiplicar() {
        resultadoObtenido = calc.multiplicar(a,b);
        Assertions.assertEquals(resultadoEsperadoMultiplicacion, calc.multiplicar(a,b));
    }

    @Test
    void dividirPorCero() {
        b=0;
        Assertions.assertEquals(calc.dividir(a,b), mensajeEsperado);
    }
}