package cadenaUtilTest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tareas.dos.cadenaUtil.CadenaUtil;

class CadenaUtilTest {

    String input1="reconocer";
    String input2=" ";
    String input3="Roma";
    CadenaUtil miCadena = new CadenaUtil();

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void invertirNoNula() {
        Assertions.assertEquals("amoR", miCadena.invertir(input3));
    }
    @Test
    void invertirNula(){
        Assertions.assertNull(null, miCadena.invertir(input2));
    }

    @Test
    void esPalindromo() {
        Assertions.assertTrue(miCadena.esPalindromo(input1));
    }

    @Test
    void noEsPalindromo(){
        Assertions.assertFalse(miCadena.esPalindromo(input3));
    }

    @Test
    void nulaPalindromo(){
        Assertions.assertTrue(miCadena.esPalindromo(input2));
    }
}