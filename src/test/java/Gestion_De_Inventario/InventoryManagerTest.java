package Gestion_De_Inventario;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tareas.tres.Gestion_De_Inventario.InventoryManager;

import static org.junit.jupiter.api.Assertions.*;

class InventoryManagerTest {
    InventoryManager inv = new InventoryManager();
    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addProduct() {
        Assertions.assertTrue(inv.addProduct("1","Pastafrola",1,1));
    }
}