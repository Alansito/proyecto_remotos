package Gestion_de_Tareas;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tareas.tres.Gestion_De_Tareas.Task;
import tareas.tres.Gestion_De_Tareas.TaskManager;

import java.util.List;

class TaskManagerTest {
    TaskManager manager = new TaskManager();
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }
//AÑADIR TAREAS
    @Test
    void casoFeliz() {
        Assertions.assertTrue(manager.addTask("1","Cocinar repollo"));
    }
    @Test
    void add_duplicado(){
        manager.addTask("1","Cocinar repollo");
        Assertions.assertFalse(manager.addTask("1","Cocinar repollo"));
    }
    @Test
    void list_valid(){
        manager.addTask("1","Cocinar repollo");
        Assertions.assertNotNull(manager.listTasks());

    }
//COMPLETAR TAREAS
    @Test
    void casoFeliz1(){
        manager.addTask("1","Cocinar repollo");
        Assertions.assertTrue(manager.completeTask("1"));

    }
    @Test
    void completar_completada(){
        manager.addTask("1","Cocinar repollo");
        manager.completeTask("1");
        Assertions.assertFalse(manager.completeTask("1"));
    }
    @Test
    void completar_inexistente(){
        Assertions.assertFalse(manager.completeTask("1"));
    }
    @Test
    void lista_completadas(){
        manager.addTask("1","Cocinar repollo");
        manager.completeTask("1");
        Assertions.assertNotNull(manager.listCompletedTasks());
    }
//ELIMINACION DE TAREAS
    @Test
    void casoFeliz2 (){
        manager.addTask("1","Cocinar repollo");
        Assertions.assertTrue(manager.deleteTask("1"));
    }
    @Test
    void eliminar_eliminada(){
        manager.addTask("1","Cocinar repollo");
        manager.deleteTask("1");
        Assertions.assertFalse(manager.deleteTask("1"));
    }
    /*@Test
    void verificar_lista_eliminada(){
        manager.addTask("1","Cocinar repollo");
        manager.deleteTask("1");
        Assertions.assertNull(manager.listTasks()); Preguntar al profe
    }*/
    //PRUEBA DE LISTADOS
    @Test
    void casoFeliz3(){
        manager.addTask("1","Cocinar repollo");
        Assertions.assertNotNull(manager.listTasks());
    }
    @Test
    void lista_completadas_valid(){
        manager.addTask("1","Cocinar repollo");
        manager.addTask("2","Cocinar mortadela");
        manager.completeTask("2");
        Assertions.assertNotNull(manager.listCompletedTasks());
    }
    @Test
    void tareas_pendientes_valid(){
        manager.addTask("1","Cocinar repollo");
        manager.addTask("2","Cocinar mortadela");
        manager.completeTask("2");
        List<Task> tareasPendientes = manager.listPendingTasks();
        Assertions.assertEquals(1,tareasPendientes.size());
        Assertions.assertNotEquals(manager.listCompletedTasks(),manager.listPendingTasks());
    }
}