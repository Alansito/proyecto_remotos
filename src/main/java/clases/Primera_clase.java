package clases;

public class Primera_clase {

    //ATRIBUTOS (variables)
    String nombre = "calculadora";
    static int numero_uno = 1;
    static int numero_dos = 2;
    static double resultado;
    static double numero_tres = 0.5;
    static double numero_cuatro = 1.5;

    //FUNCIONES
    public static void sumar() {
        resultado = numero_uno + numero_dos;
        System.out.println("el resultado es " + resultado);
    }

    public static void multiplicar() {
        resultado = numero_uno * numero_dos;
        System.out.print("el resultado es " + resultado);
    }

    public static void division() {
        resultado =(double) numero_uno / numero_dos;
        System.out.println("el resultado es " + resultado);
    }

    public static void resta() {
        resultado = numero_uno - numero_dos;
        System.out.println("el resultado es " + resultado);
    }

    public static void suma_decimales() {
        resultado = numero_cuatro + numero_tres;
        System.out.print("el resultado es " + resultado);

    }

    public static void main(String[] args) {
        suma_decimales();
    }
}
