package clases;

import java.util.Scanner;

public class segunda_clase {

    //DEF VARIABLES
    static String modelo = "Cassio";
    static double n1;
    static double n2;
    static double resultado;
    static String operacion;
    //OBJETO
    static Scanner scanner = new Scanner(System.in);

    //DEF FUNCIONES
    public static void suma(double n1, double n2) {
        resultado = n1 + n2;
    }

    private static void restar(double n1, double n2) {
        resultado = n1 - n2;
    }

    private static void dividir(double n1, double n2) {
        resultado = n1 / n2;
    }

    private static void multiplicar(double n1, double n2) {
        resultado = n1 * n2;
    }

    public static void main(String[] args) {
        System.out.println("Bienvenido a la calculadora " + modelo);
        System.out.println("Seleccione operacion");
        operacion = scanner.next();
        seleccionar_operacion();
        System.out.print("el resultado es: " + resultado);
        suma(n1, n2);
        System.out.print("el resultado es: " + resultado);
        ingresar_valores();
        restar(n1, n2);
        System.out.print("el resultado es: " + resultado);
        ingresar_valores();
        dividir(n1, n2);
        System.out.print("el resultado es: " + resultado);
        multiplicar(n1, n2);
        ingresar_valores();
        System.out.print("el resultado es: " + resultado);
    }

    private static void seleccionar_operacion() {
        if (operacion.equals("sumar")) {
            ingresar_valores();
            suma(n1, n2);
        } else if (operacion.equals("resta")) {
            ingresar_valores();
            restar(n1, n2);
        } else if (operacion.equals("dividir")) {
            ingresar_valores();
            dividir(n1, n2);
        } else if (operacion.equals("multiplicar")) {
            ingresar_valores();
            multiplicar(n1, n2);
        } else {
            System.out.println("nulo");
        }
    }


    private static void ingresar_valores() {
        System.out.println("Ingrese primer numero...");
        n1 = scanner.nextInt();
        System.out.print("ingrese segundo numero...");
        n2 = scanner.nextInt();
    }

}
