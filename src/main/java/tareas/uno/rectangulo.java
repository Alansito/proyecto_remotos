package tareas.uno;

import java.util.Scanner;

public class rectangulo {
    static double l;
    static double h;
    static double r;
    static int eleccion;
    static boolean encendido = false;

    static Scanner entrada = new Scanner(System.in);

    public static void perimetro(double l, double h) {
        r = 2 * l + 2 * h;
    }

    public static void area(double l, double h) {
        r = l * h;
    }

    public static void main(String[] args) {
        if (!encendido) {
            encendido = true;
        } else {
            System.out.println("//Bienvenido a la calculadora de areas y perimetros de RECTANGULOS//");
        }
        while(encendido) {
            System.out.println("Ingrese longitud del rectángulo ");
            l = entrada.nextDouble();
            System.out.println("ingrese la altura del rectangulo ");
            h = entrada.nextDouble();
            System.out.println("Que desea calcular?");
            System.out.println("1-Area");
            System.out.println("2-Perimetro");
            System.out.println("3-apagar");
            eleccion = entrada.nextInt();
            switch (eleccion) {
                case 1:
                    area(l, h);
                    break;
                case 2:
                    perimetro(l, h);
                    break;
                case 3:
                    encendido=false;
                    break;
                default:
                    System.out.print("Invalido");
            }
            if(encendido) {
                System.out.println("el resultado es " + r);
            }
        }
    }
}
