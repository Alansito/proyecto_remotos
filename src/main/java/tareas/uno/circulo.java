package tareas.uno;

import java.util.Scanner;

public class circulo {
    static boolean encendido=false;
    static double r;
    static double a;
    static double pi=Math.PI;
    static String eleccion;
    static Scanner entrada = new Scanner(System.in);

    public static void main(String[] args) {
        encendido=true;
        System.out.println("Bienvenido a la calculadora de areas para circulos");
        while(encendido==true) {
            System.out.println("Ingrese el radio de su circulo a continuacion (m)");
            r = entrada.nextDouble();
            a = pi*r*r;
            System.out.println("el area del circulo es: " + a + "m^2");
            System.out.println("¿Desea continuar? (si/no)");
            eleccion=entrada.next();
            switch(eleccion){
                case "si":
                    break;
                case "no":
                    encendido=false;
                default:
                    System.out.println("Invalido");
                    break;
            }
        }
    }
}
