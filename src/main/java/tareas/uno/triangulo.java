package tareas.uno;

import java.util.Scanner;

public class triangulo {
    static double l_1;
    static double l_2;
    static double l_3;
    static double a = 0.0001;
    static Scanner entrada = new Scanner(System.in);
    static boolean encendido=true;

    public static void iguales(double l_1, double l_2, double l_3, double a) {
        if (Math.abs(l_1 - l_2) < a && Math.abs(l_1 - l_3) < a) {
            System.out.println("El triangulo es equilatero");
        }else if(Math.abs(l_1 - l_2) < a && (l_1!=l_3) || Math.abs(l_1-l_3)<a && (l_1!=l_2) || Math.abs(l_2-l_3)<a && (l_2!=l_1)){
            System.out.println("El triangulo es isoceles");
        }else{
            System.out.println("El triangulo es escaleno");
        }
    }

    public static void main(String[] args) {
        while(encendido) {
            System.out.println(" ");
            System.out.println("Bienvenido al clasificador de triangulos");
            System.out.println("Ingrese los tres lados de su triángulo");
            System.out.println("LADO UNO:");
            l_1 = entrada.nextDouble();
            System.out.println("LADO DOS:");
            l_2 = entrada.nextDouble();
            System.out.println("LADO TRES:");
            l_3 = entrada.nextDouble();
            System.out.println("CALCULANDO...");
            iguales(l_1, l_2, l_3, a);
            System.out.println(" ");
        }
    }
}
