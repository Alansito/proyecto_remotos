package tareas.dos.cadenaUtil;

public class CadenaUtil {
    public String invertir(String input) {
        if (input == null) {
            return null;
        }
        return new StringBuilder(input).reverse().toString();
    }

    public boolean esPalindromo(String input) {
        if (input == null) {
            return false;
        }
        String invertida = invertir(input);
        return input.equals(invertida);
    }
}
