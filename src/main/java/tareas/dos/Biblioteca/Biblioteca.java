package tareas.dos.Biblioteca;

public class Biblioteca extends Libro {
    int seleccion_usuario;
    int seleccion_libro;
    Usuario usuario = new Usuario();
    public void prestar() {
        System.out.println("Seleccione un libro:");
        for (int i = 0; i < lista_libros.size(); i++) {
            System.out.println(i + "- " + lista_libros.get(i)+" Del autor: "+ lista_autores.get(i));
        }
        seleccion=entrada.nextInt();
        if (mapa_prestados.containsKey(lista_libros.get(seleccion))) {
            System.out.println("prestado");
        } else {
            mapa_prestados.put(lista_libros.get(seleccion), true);
            System.out.println(" ");
        }
    }
    public void mostrar_libros() {
        System.out.println(" ");
        System.out.println("-----------------");
        for (int i = 0; i < lista_libros.size(); i++) {
            if (mapa_prestados.containsKey(lista_libros.get(i))) {
                System.out.println(lista_libros.get(i) + " del autor: " + lista_autores.get(i)+"  //Prestado//");
            }else if (!mapa_prestados.containsKey(lista_libros.get(i))){
                System.out.println(lista_libros.get(i) + " del autor: " + lista_autores.get(i));
            }
        }
        System.out.println("-----------------");
        System.out.println(" ");
    }

    public void devolver (){
        System.out.println("Seleccione el libro que desea devolver");
        for(int i=0; i<lista_libros.size();i++){
            System.out.println(i+"- "+lista_libros.get(i));
        }
        seleccion_libro=entrada.nextInt();
        usuario.mapa_usuarios_prestados.remove(lista_libros.get(seleccion_libro));
        mapa_prestados.remove(lista_libros.get(seleccion_libro));
    }
}
