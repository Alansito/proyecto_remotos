package tareas.dos.Biblioteca;

import java.util.HashMap;
import java.util.Map;

public class Usuario extends Persona{
    static Biblioteca biblioteca1 = new Biblioteca();
    static Map<String , Integer> mapa_usuarios_prestados = new HashMap<>();
    static int seleccion_usuario;

    public void unionLibroUsuario(){
        System.out.println("Seleccione su usuario: ");
        for(int i=0; i<lista_cedulas.size();i++){
            System.out.println(i+"- "+lista_cedulas.get(i) +" "+"("+ lista_nombres.get(i)+")");
        }
        seleccion_usuario=entrada.nextInt();
        if(biblioteca1.mapa_prestados.containsValue(true)){
            for(int i=0; i<biblioteca1.mapa_prestados.size() ; i++){
                if(biblioteca1.mapa_prestados.containsKey(biblioteca1.lista_libros.get(biblioteca1.seleccion))){
                    mapa_usuarios_prestados.put(biblioteca1.lista_libros.get(biblioteca1.seleccion), lista_cedulas.get(seleccion_usuario));
                }else{
                    System.out.println(" ");
                }
            }
        }else{
            System.out.println("no existen libros prestados");
        }
    }

    public void mostrar_libros_prestados(){
        if(mapa_usuarios_prestados.isEmpty()){
            for(int i = 0 ; i<lista_cedulas.size(); i++){
                System.out.println(lista_cedulas.get(i));
            }
        }else{
            System.out.println(mapa_usuarios_prestados);
        }
    }

}
