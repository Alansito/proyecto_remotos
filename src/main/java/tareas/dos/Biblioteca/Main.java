package tareas.dos.Biblioteca;

public class Main {
    static Biblioteca biblioteca = new Biblioteca();
    static Persona persona = new Persona();
    static Usuario usuario = new Usuario();
    static Libro libro = new Libro();
    static int opcion;

    public static void main(String[] args) {
        System.out.println("////Bienvenido////");
        System.out.println("------------------------");
        while(opcion!=7) {
            System.out.println("Seleccione una opcion:");
            System.out.println("1-Registrar usuario");
            System.out.println("2-Agregar un libro");
            System.out.println("3-Prestar un libro");
            System.out.println("4-Devolver un libro");
            System.out.println("5-Mostrar lista de libros");
            System.out.println("6-Mostrar lista de usuarios");
            System.out.println("7-Salir");
            opcion=biblioteca.entrada.nextInt();
            switch (opcion) {
                case 1:
                    persona.pedirDatos();
                    break;
                case 2:
                    libro.add_libro();
                    break;
                case 3:
                    biblioteca.prestar();
                    usuario.unionLibroUsuario();
                    break;
                case 4:
                    biblioteca.devolver();
                    break;
                case 5:
                    biblioteca.mostrar_libros();
                    break;
                case 6:
                    usuario.mostrar_libros_prestados();
                    break;
                case 7:
                    break;
            }
        }
        persona.mostrarDatos();
    }
}
