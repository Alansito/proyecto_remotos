package tareas.dos.Biblioteca;

import java.util.*;

public class Libro {
    static String nombre_libro;
    static String nombre_autor;
    static boolean prestado;
    static int seleccion;
    static List<String> lista_libros = new ArrayList<>();
    static List<String> lista_autores = new ArrayList<>();
    static Map<String, Boolean> mapa_prestados = new HashMap<>();
    static Scanner entrada = new Scanner(System.in);

    public void add_libro(){
        System.out.println("Ingrese el nombre del libro que desea agregar");
        nombre_libro=entrada.next();
        lista_libros.add(nombre_libro);
        System.out.println("Ingrese el nombre del autor");
        nombre_autor=entrada.next();
        lista_autores.add(nombre_autor);
    }
}
