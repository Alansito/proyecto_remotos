package tareas.dos.Biblioteca;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Persona {
    static String nombre;
    static int cedula;
    static List<Integer> lista_cedulas=new ArrayList<>();
    static List <String>lista_nombres=new ArrayList<>();
    static Scanner entrada = new Scanner(System.in);

    public void pedirDatos() {
        System.out.println("Ingrese su nombre");
        nombre=entrada.next();
        System.out.println("Ingrese su cédula (no guiones ni puntos)");
        cedula=entrada.nextInt();
        if(nombre!=null && cedula>0){
            System.out.println("Usuario ingresado con exito");
            lista_cedulas.add(cedula);
            lista_nombres.add(nombre);
        }else{
            System.out.println("Ocurrio un error al ingresar el usuario, intente nuevamente");
            lista_cedulas.remove(cedula);
            pedirDatos();
        }
    }
    public void mostrarDatos(){
        for(int i=0; i<lista_cedulas.size();i++){
            System.out.println(i+". "+"Nombre: "+lista_nombres.get(i)+" C.I: "+lista_cedulas.get(i));
        }
    }
}
