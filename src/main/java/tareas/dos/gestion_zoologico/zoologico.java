package tareas.dos.gestion_zoologico;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class zoologico extends ave {
    static List<String> lista_animales = new ArrayList<>();
    static List<Double> lista_pesos = new ArrayList<>();
    static Scanner entrada = new Scanner(System.in);
    static HashMap<String, Integer> mapa_animales = new HashMap<>();
    static String decision;
    static boolean esAve;
    static boolean agregando=true;

    public static void agregarAnimal() {
        while(agregando) {
            System.out.println("Agregue un animal a la lista");
            nombre = entrada.next();
            lista_animales.add(nombre);
            System.out.println("el animal es un ave?(si/no)");
            decision = entrada.next();
            switch (decision) {
                case "si":
                    esAve = true;
                    System.out.println("ingrese el peso del animal");
                    peso = entrada.nextDouble();
                    lista_pesos.add(peso);
                    break;
                case "no":
                    System.out.println("Ingrese el peso del animal");
                    peso = entrada.nextDouble();
                    System.out.println("el animal es carnivoro?(si/no)");
                    decision = entrada.next();
                    if (decision.equals("si")) {
                        esCarnivoro = true;
                    } else if (decision.equals("no")) {
                        esCarnivoro = false;
                    } else {
                        System.out.println("error");
                    }
                    lista_pesos.add(peso);
                    break;
                default:
                    break;
            }
            System.out.println("Desea agregar otro animal? (si/no)");
            decision=entrada.next();
            if(decision.equals("no")){
                agregando=false;
            }else{
                System.out.println("...");
            }
        }
    }

    public static void mostrarAlimentacionDiaria() throws InterruptedException {
        System.out.println(" ");
        if(esAve){
            for(int i=0; i<lista_animales.size(); i++) {
                System.out.println(lista_animales.get(i)+" "+lista_pesos.get(i)+"kg"+" //Alimentacion necesaria: "+calcularAlimentacionDiariaAve(lista_pesos.get(i)));
            }
        }else{
            for(int i=0; i<lista_animales.size(); i++) {
                System.out.println(lista_animales.get(i)+" "+lista_pesos.get(i)+"kg"+" //Alimentacion necesaria: "+calcularAlimentacionDiaria(lista_pesos.get(i)));
            }
        }
        System.out.println(" ");
        Thread.sleep(1000);
    }
}
