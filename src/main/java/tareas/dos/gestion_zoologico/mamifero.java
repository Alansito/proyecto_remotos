package tareas.dos.gestion_zoologico;

public class mamifero extends animal {
    static boolean esCarnivoro;

    public static double calcularAlimentacionDiaria(double peso) {
        if (esCarnivoro) {
            return peso * 0.05;
        } else if (!esCarnivoro) {
            return peso * 0.03;
        }else{
            return 0;
        }
    }
}


