package tareas.dos.gestion_zoologico;

public class main {
    static zoologico mizoo = new zoologico();
    static int opciones;
    static boolean encendido=false;

    public static void main(String[] args) throws InterruptedException {
        if(!encendido){
            encendido=true;
            while(encendido) {
                System.out.println("//ZOOLOGICO//");
                System.out.println("Seleccione una opcion:");
                System.out.println("1-Agregar animales a la lista");
                System.out.println("2-Mostrar lista de animales");
                System.out.println("3-Salir");
                opciones=mizoo.entrada.nextInt();
                switch(opciones) {
                    case 1:
                        mizoo.agregarAnimal();
                        break;
                    case 2:
                        if(mizoo.lista_animales.isEmpty()){
                            System.out.println(" ");
                            System.out.println("/ERROR/");
                            System.out.println("-la lista esta vacía");
                            System.out.println(" ");
                        }else{
                            mizoo.mostrarAlimentacionDiaria();
                        }
                        break;
                    case 3:
                        encendido=false;
                        break;
                    default:
                        System.out.println("error");
                        break;
                }
            }
        }else{
            System.out.println("ERROR");
        }
    }
}
