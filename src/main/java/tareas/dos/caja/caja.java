package tareas.dos.caja;

import java.util.Scanner;

public class caja {
    static double l;
    static double p;
    static double h;
    static double r;
    static boolean power=false;
    static int eleccion1;
    static Scanner entrada=new Scanner(System.in);

    public static void v (){
        r=l*p*h;
    }
    public static void a(){
        r=l*p;
    }

    public static void main(String[] args) {
        if(!power){
            power=true;
        }else{
            System.out.println("!ERROR¡");
        }
        System.out.println("Bienvenido a la calculadora de areas y volumenes de cajas...");
        while(power){
            System.out.println("Ingrese la longitud de su caja (m)");
            l=entrada.nextDouble();
            System.out.println("Ingrese el ancho de su caja (m)");
            p=entrada.nextDouble();
            System.out.println("Ingrese la altura de su caja (m)");
            h=entrada.nextDouble();
            System.out.println("¿Que desea calcular?");
            System.out.println("1-Area");
            System.out.println("2-Volumen");
            eleccion1=entrada.nextInt();
            switch(eleccion1){
                case 1:
                    a();
                    System.out.println("El area de su caja es de: "+r+"m^2");
                    break;
                case 2:
                    v();
                    System.out.println("El volumen de la caja es de: "+r+"m^3");
                    break;
                default:
                    System.out.println("Invalido");
                    break;
            }
            System.out.println("¿Desea seguir calculando?");
            System.out.println("1-Si");
            System.out.println("2-No");
            eleccion1=entrada.nextInt();
            switch(eleccion1){
                case 1:
                    break;
                case 2:
                    System.out.println("//Muchas gracias//");
                    power=false;
                    break;
                default:
                    System.out.println("Invalido");
                    break;
            }
        }
    }
}
