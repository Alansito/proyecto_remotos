package tareas.dos.gestion_vehiculos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class garaje extends motocicleta {
    static List<String> vehiculos =new ArrayList<>();
    static List<String> modelos =new ArrayList<>();
    static Map<String,String> mapa_vehiculos=new HashMap<>();
    static int opcion;
    static int opcion2;
    public static void agregarVehiculo(){
        System.out.println("1-coche");
        System.out.println("2-motocicleta");
        opcion=entrada.nextInt();
        while(opcion==1 || opcion==2) {
            switch(opcion) {
                case 1:
                    System.out.println("ingrese marca de su vehiculo");
                    marca=entrada.next();
                    vehiculos.add(marca);
                    System.out.println("ingrese modelo de su vehiculo");
                    modelo=entrada.next();
                    System.out.println("ingrese numero de puertas");
                    numPuertas=entrada.nextInt();
                    calcularCostoMantenimientoCoche();
                    costos.add(costo);
                    modelos.add(modelo);
                    for (int i = 0; i<vehiculos.size(); i++){
                        mapa_vehiculos.put(vehiculos.get(i)+" "+modelos.get(i)," "+costos.get(i)+"$");
                    }
                    System.out.println("desea agregar otro vehiculo?");
                    System.out.println("1-si");
                    System.out.println("2-no");
                    opcion=entrada.nextInt();
                    if(opcion==1){
                        opcion=1;
                    }else if(opcion==2){
                        opcion=0;
                    }
                    break;
                case 2:
                    System.out.println("ingrese marca de su vehiculo");
                    marca=entrada.next();
                    vehiculos.add(marca);
                    System.out.println("ingrese modelo de su vehiculo");
                    modelo=entrada.next();
                    System.out.println("ingrese cilindrada de la moto");
                    cilindrada=entrada.nextInt();
                    calcularCostoMantenimientoMoto();
                    costos.add(costo);
                    modelos.add(modelo);
                    for (int i = 0; i<vehiculos.size(); i++){
                        mapa_vehiculos.put(vehiculos.get(i)+" "+modelos.get(i)," "+costos.get(i)+"$");
                    }
                    System.out.println("desea agregar otro vehiculo?");
                    System.out.println("1-si");
                    System.out.println("2-no");
                    opcion=entrada.nextInt();
                    if(opcion==1){
                        opcion=2;
                    }else if(opcion==2){
                        opcion=0;
                    }
                    break;
                default:
                    System.out.println("error");
                    break;
            }
        }
    }
    public static void eliminarVehiculo(){
        System.out.println("lista de vehiculos");
        for (int i=0; i<vehiculos.size();i++){
            System.out.println(i+"."+vehiculos.get(i)+" "+modelos.get(i));
        }
        System.out.println(" ");
        System.out.println("Que vehiculo desea eliminar? (ingrese posicion)");
        opcion2=entrada.nextInt();
        String claveAEliminar=vehiculos.get(opcion2)+" "+modelos.get(opcion2);
        mapa_vehiculos.remove(claveAEliminar);
        vehiculos.remove(opcion2);
        modelos.remove(opcion2);
        costos.remove(opcion2);
        System.out.println("//vehiculo eliminado//");
        System.out.println(mapa_vehiculos);
    }
    public static void mostrarCostosMantenimiento(){
        System.out.println(mapa_vehiculos);
    }
}

