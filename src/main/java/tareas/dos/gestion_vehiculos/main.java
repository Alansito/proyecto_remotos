package tareas.dos.gestion_vehiculos;

public class main {
    static garaje migaraje = new garaje();
    static boolean prendido1=false;
    static int opcion;

    public static void main(String[] args) {
        if(prendido1==false){
            System.out.println("Bienvenido");
            prendido1=true;
        }else{
            System.out.println("ERROR");
        }
        while(prendido1) {
            System.out.println("Seleccione una de las siguientes opciones");
            System.out.println("1-Agregar vehiculo");
            System.out.println("2-Eliminar vehiculo");
            System.out.println("3-mostrar costo de mantenimiento");
            opcion=migaraje.entrada.nextInt();
            switch(opcion){
                case 1:
                    migaraje.agregarVehiculo();
                    break;
                case 2:
                    migaraje.eliminarVehiculo();
                    break;
                case 3:
                    migaraje.mostrarCostosMantenimiento();
                    break;
            }
        }
    }
}
