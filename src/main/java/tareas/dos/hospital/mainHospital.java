package tareas.dos.hospital;

public class mainHospital {
    static paciente miPaciente = new paciente();
    static boolean encendido=true;
    public static void main(String[] args) {
        System.out.println("Bienvenido al hospital Alansito's health");
        while(encendido) {
            System.out.println("Seleccione una operacion: ");
            System.out.println("//Camas disponibles:" +miPaciente.camas_disp+ "//");
            System.out.println("1-Registrar paciente");
            System.out.println("2-Reservar cama");
            System.out.println("3-Dar de alta");
            System.out.println("4-Apagar");
            miPaciente.eleccion = miPaciente.entrada.nextInt();
            switch (miPaciente.eleccion) {
                case 1:
                    miPaciente.registro();
                    break;
                case 2:
                    if(miPaciente.nombre!=null) {
                        miPaciente.solicitar_cama();
                    }else{
                        System.out.println("ERROR");
                        System.out.println("Usted no ha ingresado a ningun paciente");
                        System.out.println("Ingrese a un paciente para poder reservar camas");
                        System.out.println(" ");
                    }
                    break;
                case 3:
                    if(miPaciente.nombre!=null) {
                        miPaciente.alta();
                    }else{
                        System.out.println("ERROR");
                        System.out.println("No hay pacientes para dar de alta");
                    }
                    break;
                case 4:
                    System.out.println("Adios");
                    encendido=false;
                    break;
                default:
                    System.out.println("Invalido");
                    break;

            }
        }
    }
}
