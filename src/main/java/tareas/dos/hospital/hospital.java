package tareas.dos.hospital;

import java.util.Scanner;

public class hospital {
    static int camas_disp=10;
    static int camas_solicitadas;
    static Scanner entrada=new Scanner(System.in);

    public static void solicitar_cama (){
        System.out.println("¿Cuantas camas desea solicitar?");
        camas_solicitadas=entrada.nextInt();
        camas_disp=camas_disp-camas_solicitadas;
        if(camas_disp<0){
            System.out.println("No hay suficientes camas disponibles");
            camas_disp=camas_disp+camas_solicitadas;
            camas_solicitadas=0;
        }else{
            System.out.println("Usted ha solicitado "+camas_solicitadas+" camas.");
        }
    }
}
