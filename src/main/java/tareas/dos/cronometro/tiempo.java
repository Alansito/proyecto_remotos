package tareas.dos.cronometro;

import java.util.Scanner;

public class tiempo {
    static long inicio = 0;
    static long start ;
    static long tiempo_total;
    static Scanner entrada = new Scanner(System.in);


    public static void iniciar() {
        inicio =System.currentTimeMillis()-start;
        tiempo_total=inicio;
    }

    public static String formatElapsedTime(long tiempo_total) {
        long horas = tiempo_total / 36000000;
        long minutos = (tiempo_total % 3600000) / 60000;
        long segundos = ((tiempo_total % 3600000) % 60000) / 1000;
        long milisegundos = tiempo_total % 1000;
        return String.format("%02d:%02d:%02d:%03d", horas, minutos, segundos, milisegundos);
    }
}
