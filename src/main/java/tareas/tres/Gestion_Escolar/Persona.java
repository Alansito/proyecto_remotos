package tareas.tres.Gestion_Escolar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Persona {
    String nombre;
    int edad;
    List<String> profesores=new ArrayList<>();
    List<String> estudiantes=new ArrayList<>();
    HashMap<String,String> materias=new HashMap<>();
    HashMap<String,String>grados=new HashMap<>();
    Scanner scanner = new Scanner(System.in);

    public void mostrarInfo() throws InterruptedException {
        System.out.println("Seleccione una opcion");
        System.out.println("1-Mostrar lista de alumnos");
        System.out.println("2-Mostrar lista de profesores");
        System.out.println("3-Buscar informacion de profesor");
        System.out.println("4-Buscar informacion de estudiante");
        int opcion = scanner.nextInt();
        switch(opcion){
            case 1:
                if(!estudiantes.isEmpty()){
                    System.out.println(estudiantes);
                }else{
                    System.out.println("No hay estudiantes añadidos a la lista");
                }
                break;
            case 2:
                if(!profesores.isEmpty()){
                    System.out.println(profesores);
                }else{
                    System.out.println("No hay profesores añadidos a la lista");
                }
                break;
            case 3:
                System.out.println("Ingrese nombre de profesor");
                String input = scanner.next();
                System.out.println(materias.get(input));
                break;
            case 4:
                System.out.println("Ingrese nombre del estudiante:");
                input=scanner.next();
                System.out.println(grados.get(input));
                break;
            default:
                break;
        }
        Thread.sleep(2000);
    }
}
