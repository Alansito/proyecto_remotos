package tareas.tres.Gestion_Escolar;

import java.util.HashMap;

public class Estudiante extends Profesor {
    String grado;

    public void add_info(){
        System.out.println("ingrese nombre del estudiante");
        nombre=scanner.next();
        estudiantes.add(nombre);
        System.out.println("ingrese edad del estudiante");
        edad=scanner.nextInt();
        System.out.println("Ingrese grado del estudiante:");
        grado=scanner.next();
        grados.put(nombre,edad+" años | grado: "+grado);
    }
    public void act_infoE(){
        System.out.println(estudiantes);
        System.out.println("Ingrese nombre del estudiante:");
        String eleccion=scanner.next();
        System.out.println("Actualizar:");
        System.out.println("1-Nombre");
        System.out.println("2-Informacion");
        int opcion=scanner.nextInt();
        switch(opcion){
            case 1:
                act_nombreE(eleccion);
                break;
            case 2:
                System.out.println(grados.get(eleccion));
                System.out.println("Ingrese nueva edad");
                String edad_new=scanner.next();
                System.out.println("Ingrese nuevo grado");
                String grado_new=scanner.next();
                grados.replace(eleccion,edad_new+" años | grado: "+ grado_new);
                break;
        }
    }
    public void act_nombreE(String eleccion){
        System.out.println("Ingrese nuevo nombre");
        String eleccion1=scanner.next();
        estudiantes.remove(eleccion);
        estudiantes.add(eleccion1);
        grados.put(eleccion1,grados.get(eleccion));
        grados.remove(eleccion);
    }

    public void rm_stud(){
        System.out.println("Ingrese estudiante que desea eliminar");
        System.out.println(estudiantes);
        String eleccion=scanner.next();
        estudiantes.remove(eleccion);
        grados.remove(eleccion);
    }
}
