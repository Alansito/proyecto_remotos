package tareas.tres.Gestion_Escolar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Profesor extends Persona{
    String materia;

    public void add_mat(){
        System.out.println("Ingrese nombre del profesor");
        nombre=scanner.next();
        profesores.add(nombre);
        System.out.println("Ingrese la materia del profesor");
        materia=scanner.next();
        materias.put(nombre,"Materia(s): "+materia);
    }
    public void act_infoP(){
        System.out.println(profesores);
        System.out.println("Ingrese nombre del profesor:");
        String eleccion = scanner.next();
        System.out.println("Actualizar:");
        System.out.println("1-Nombre");
        System.out.println("2-Informacion");
        int opcion = scanner.nextInt();
        switch(opcion){
            case 1:
                System.out.println("Ingrese el nombre nuevo:");
                String eleccion2=scanner.next();
                act_nombreP(eleccion,eleccion2);
                break;
            case 2:
                System.out.println(materias.get(eleccion));
                System.out.println("Ingrese informacion nueva");
                eleccion2=scanner.next();
                materias.replace(eleccion,"Materia(s): "+eleccion2);
                break;
            default:
                break;
        }
    }
    public void act_nombreP(String eleccion, String eleccion2){
        profesores.remove(eleccion);
        profesores.add(eleccion2);
        materias.put(eleccion2,materias.get(eleccion));
        materias.remove(eleccion);
    }
    public void rm_prof(){
        System.out.println("Ingrese el profesor que desea eliminar:");
        System.out.println(profesores);
        String eleccion=scanner.next();
        materias.remove(eleccion);
        profesores.remove(eleccion);
    }
}
