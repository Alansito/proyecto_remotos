package tareas.tres.Gestion_Escolar;

import java.sql.SQLOutput;

public class Clase_principal {
    static Estudiante mistud = new Estudiante();
    static boolean encendido=true;

    public static void main(String[] args) throws InterruptedException {
        while (encendido) {
            System.out.println("//GESTION ESCOLAR//");
            System.out.println("------------------------");
            System.out.println("1-AGREGAR ESTUDIANTE");
            System.out.println("------------------------");
            System.out.println("2-AGREGAR PROFESOR");
            System.out.println("------------------------");
            System.out.println("3-ELIMINAR ESTUDIANTE");
            System.out.println("------------------------");
            System.out.println("4-ELIMINAR PROFESOR");
            System.out.println("------------------------");
            System.out.println("5-ACTUALIZAR INFORMACION");
            System.out.println("------------------------");
            System.out.println("6-MOSTRAR INFORMACION");
            System.out.println("------------------------");
            System.out.println("7-APAGAR");
            System.out.println("------------------------");
            int opcion = mistud.scanner.nextInt();
            switch (opcion) {
                case 1:
                    mistud.add_info();
                    break;
                case 2:
                    mistud.add_mat();
                    break;
                case 3:
                    mistud.rm_stud();
                    break;
                case 4:
                    mistud.rm_prof();
                    break;
                case 5:
                    System.out.println("1-Estudiantes");
                    System.out.println("2-Profesores");
                    int input=mistud.scanner.nextInt();
                    switch(input){
                        case 1:
                            mistud.act_infoE();
                            break;
                        case 2:
                            mistud.act_infoP();
                            break;
                    }
                    break;
                case 6:
                    mistud.mostrarInfo();
                    break;
                case 7:
                    encendido=false;
                    break;
            }
        }
    }
}
