package tareas.tres.Gestion_concesionario;

public class Vehiculo {
    String numChasis;
    String marca;
    String modelo;
    boolean vendido;

    public Vehiculo(String pNumChasis, String pMarca, String pModelo, boolean pVendido){
        this.numChasis=pNumChasis;
        this.marca=pMarca;
        this.modelo=pModelo;
        this.vendido=pVendido;
    }

    public String getNumChasis(){
        return numChasis;
    }
    public String getMarca(){
        return marca;
    }
    public String getModelo(){
        return modelo;
    }
    public boolean isVendido(){
        return vendido;
    }
}
