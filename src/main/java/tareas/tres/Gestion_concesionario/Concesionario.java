package tareas.tres.Gestion_concesionario;

import org.checkerframework.checker.units.qual.A;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Concesionario {
    List<Vehiculo> vehiculos = new ArrayList<>();
    List<Cliente> clientes = new ArrayList<>();
    Map<String,Cliente>clienteMap;
    Map<String,Vehiculo> vehiculoMap;
    Map<Cliente,Vehiculo>clienteVehiculoMap;

    public Concesionario(){
        this.vehiculos = new ArrayList<>();
        this.clientes=new ArrayList<>();
        this.clienteMap=new HashMap<>();
        this.vehiculoMap=new HashMap<>();
        this.clienteVehiculoMap=new HashMap<>();
    }
    public void agregarVehiculo(Vehiculo vehiculo){
        vehiculos.add(vehiculo);
        vehiculoMap.put(vehiculo.getNumChasis(),vehiculo);
    }
    public void eliminarVehiculo(String numChasis){
        Vehiculo vehiculo = vehiculoMap.remove(numChasis);
        if (vehiculo!=null){
            vehiculos.remove(vehiculo);
        }
    }

    public List<Vehiculo> listarVehiculo(){
        return new ArrayList<>(vehiculos);
    }
    public List<Vehiculo>listarVehiculosVendidos(){
        List<Vehiculo>vehiculosVendidos=new ArrayList<>();
        for(Vehiculo vehiculo: vehiculos){
            if(vehiculo.isVendido()){
                vehiculosVendidos.add(vehiculo);
                //clienteVehiculoMap.put(vehiculosVendidos,clientes.get())
            }
        }
        return new ArrayList<>(vehiculosVendidos);
    }
    public void agregarClientes(Cliente cliente){
        clientes.add(cliente);
        clienteMap.put(cliente.getId(),cliente);
    }
    public void eliminarCliente(String id){
        Cliente cliente = clienteMap.remove(id);
        if(clientes!=null){
            clientes.remove(id);
        }
    }
    public List<Cliente>listarCliente(){
        return new ArrayList<>(clientes);
    }
    public Cliente buscarClientId(String id){
        return clienteMap.get(id);
    }
}
