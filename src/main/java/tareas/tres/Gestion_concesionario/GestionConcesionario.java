package tareas.tres.Gestion_concesionario;

public class GestionConcesionario {

    static Concesionario conc = new Concesionario();

    public static void main(String[] args) {
        conc.agregarVehiculo(new Vehiculo("12345T", "Audi","R8", true));
        conc.agregarVehiculo(new Vehiculo("67845T", "Toyota","Corona", false));
        conc.agregarClientes(new Cliente("01","Pepe"));
        conc.agregarClientes(new Cliente("02","Ramon"));


        for(Cliente cliente:conc.listarCliente()){
            System.out.println("Los clientes son:"+cliente.getId()+" - "+cliente.getNombre());
        }


        for(Vehiculo vehiculo:conc.listarVehiculo()){
            System.out.println("Los vehiculos son:" + vehiculo.getMarca()+"-"+vehiculo.getModelo()+"-"+vehiculo.numChasis);
        }

        for(Vehiculo vehiculo:conc.listarVehiculosVendidos()){
            System.out.println("Los vehiculos vendidos son :"+vehiculo.getMarca()+"-"+vehiculo.getModelo()+"-"+vehiculo.isVendido());
        }
    }
}
