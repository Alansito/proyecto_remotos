package tareas.tres.Gestion_concesionario;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
    String id;
    String nombre;
    List<Vehiculo> vehiculosComprados;

    public Cliente (String pId, String pNombre){
        this.id=pId;
        this.nombre=pNombre;
        this.vehiculosComprados=new ArrayList<>();
    }
    public String getId(){
        return id;
    }
    public String getNombre(){
        return nombre;
    }
    public List<Vehiculo>getVehiculosComprados(){
        return vehiculosComprados;
    }
    public void comprarVehiculos(Vehiculo vehiculo){
        vehiculosComprados.add(vehiculo);
    }
    public void devolverVehiculos(Vehiculo vehiculo){
        vehiculosComprados.remove(vehiculo);
    }

}
