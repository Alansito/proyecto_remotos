package tareas.tres.Gestion_De_Tareas;

public class ClasePrincipal{

    public static void main(String[] args) {
        TaskManager manager = new TaskManager();
        manager.addTask("1", "Write unit tests");
        manager.addTask("2", "Review PRs");

        System.out.println("All tasks: " + manager.listTasks());
        System.out.println("Pending tasks: " + manager.listPendingTasks());

        manager.completeTask("1");
        System.out.println("Completed tasks: " + manager.listCompletedTasks());
        System.out.println("Pending tasks: " + manager.listPendingTasks());

        manager.deleteTask("2");
        System.out.println("All tasks after deletion: " + manager.listTasks());
    }
}
