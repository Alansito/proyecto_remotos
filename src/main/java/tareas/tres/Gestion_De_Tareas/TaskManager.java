package tareas.tres.Gestion_De_Tareas;

import java.util.*;

public class TaskManager {
    private Map<String, Task> tasks = new HashMap<>();

    public boolean addTask(String id, String description) {
        if (tasks.containsKey(id)) {
            return false;
        }
        tasks.put(id, new Task(id, description));
        return true;
    }

    public boolean completeTask(String id) {
        Task task = tasks.get(id);
        if (task == null || task.isCompleted()) {
            return false;
        }
        task.setCompleted(true);
        return true;
    }

    public boolean deleteTask(String id) {
        return tasks.remove(id) != null;
    }

    public List<Task> listTasks() {
        return new ArrayList<>(tasks.values());
    }

    public List<Task> listCompletedTasks() {
        List<Task> completedTasks = new ArrayList<>();
        for (Task task : tasks.values()) {
            if (task.isCompleted()) {
                completedTasks.add(task);
            }
        }
        return completedTasks;
    }

    public List<Task> listPendingTasks() {
        List<Task> pendingTasks = new ArrayList<>();
        for (Task task : tasks.values()) {
            if (!task.isCompleted()) {
                pendingTasks.add(task);
            }
        }
        return pendingTasks;
    }
}
