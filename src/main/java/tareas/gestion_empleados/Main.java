package tareas.gestion_empleados;

public class Main {
    static Empresa miEmpresa = new Empresa();
    static int opcion1;
    static int opcion2;

    public static void main(String[] args) {

        System.out.println("//Bienvenido//");
        System.out.println("Seleccione una opcion:");
        System.out.println("1-Agregar nombre de empleado");
        System.out.println("2-Calcular salario total");
        System.out.println("3-Calcular salario de empleado");
        opcion1 = miEmpresa.entrada.nextInt();
        switch (opcion1) {
            case 1:
                miEmpresa.agregarEmpleado();
                break;
            case 2:
                if (miEmpresa.nombre == "") {
                    System.out.println("error");
                } else {
                    miEmpresa.calcularSalariosTotales();
                }
                break;
            case 3:
                break;
        }
    }
}
